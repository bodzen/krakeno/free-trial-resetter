FROM python:3.8.5-slim-buster

COPY requirements.txt /opt/requirements.txt

RUN /usr/bin/apt-get update \
	&& /usr/bin/apt list --upgradable \
	&& /usr/bin/apt upgrade -yy \
	&& /usr/bin/apt -qq install -yy gcc libpq-dev \
	&& pip3 install --upgrade pip \
	&& pip3 install -r /opt/requirements.txt \
	&& rm -f /opt/requirements.txt \
	&& apt purge gcc -yy

COPY free-trial-resetter /opt/apt/free-trial-resetter

USER 1000:1000
WORKDIR /opt/apt/free-trial-resetter
ENV PYTHONUNBUFFERED=1
ENTRYPOINT ["python", "resetter.py"]
