#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os

try:
    os.environ['DEV_ENV']
    SQLALCHEMY_DATABASE_URI = 'postgres://postgres:mysecretpassword@172.18.0.3'
except KeyError:
    SQLALCHEMY_DATABASE_URI = os.environ['POSTGRESQL_URI']

try:
    SENTRY_URI = os.environ['SENTRY_URI']
except KeyError:
    SENTRY_URI = None
