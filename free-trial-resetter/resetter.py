#!/usr/bin/python3
# -*- coding: utf-8 -*-

from sqlalchemy import create_engine
from config import SQLALCHEMY_DATABASE_URI
from init_sentry import init_sentry


def db_connection_wrapper(func):
    """
        Decorator to manage db connection:
            1. init connection
            2. pass it's object to wrapped function
            3. properly close de connection
        return: wrapped function's result
    """
    def wrapper(*args, **kwargs):
        engine = create_engine(SQLALCHEMY_DATABASE_URI, echo=False)
        db_conn = engine.connect()
        res = func(*args, **kwargs, db_conn=db_conn)
        db_conn.close()
        engine.dispose()
        return res
    return wrapper


@db_connection_wrapper
def reset_free_accounts(db_conn=None):
    query = """
        UPDATE users
            SET dl_left = 15
        WHERE users.status = 'free' AND
            users.dl_left < 15 AND
            users.created_at >= date_trunc('week', CURRENT_TIMESTAMP - interval '1 week');
    """
    db_conn.execute(query)


@db_connection_wrapper
def reset_anonymous(db_conn=None):
    query = """
        UPDATE anonymous_users
            SET dl_left = 5
        WHERE anonymous_users.created_at >= date_trunc('week', CURRENT_TIMESTAMP - interval '1 week');
    """
    db_conn.execute(query)


if __name__ in '__main__':
    init_sentry()
    reset_free_accounts()
    reset_anonymous()
